# Obfuscator-Batch

# Description

This obfuscate tool support obfuscating multiple javascript in 1 folder or directory

# Installation 

Run 'npm install' to download all required module


# Configuration

item that have to be configured

1. 
//set it true if you want the output file has the same name with the input file
var makeSameName = false;

2.
//if makeSameName true then afterfix will ignored else file will have afterfile like [FILENAME]-[AFTERFIX].js
//example myfile-obfuscated.js
// if afterfix = '' then afterfix value is obfuscated else afterfix will change its name no longer obfuscated
var afterfix = '';

3.
// set the input directory
var inputDirectory = 'C:/Users/A43E/Documents/Gitlab/Simulation-Test/Obfuscate/javascript-obfuscator/listfiles/';

4.
// set the output directory
var outputDirectory = 'C:/Users/A43E/Documents/Gitlab/Simulation-Test/Obfuscate/javascript-obfuscator/output/';

# How to use

Just simply type

node obfuscator-batch.js