//set it true if you want the output file has the same name with the input file
var makeSameName = false;

//if makeSameName true then afterfix will ignored else file will have afterfile like [FILENAME]-[AFTERFIX].js
//example myfile-obfuscated.js
// if afterfix = '' then afterfix value is obfuscated else afterfix will change its name no longer obfuscated
var afterfix = '';

// set the input directory
var inputDirectory = 'C:/Users/A43E/Documents/Gitlab/Simulation-Test/Obfuscate/javascript-obfuscator/listfiles/';
// set the output directory
var outputDirectory = 'C:/Users/A43E/Documents/Gitlab/Simulation-Test/Obfuscate/javascript-obfuscator/output/';


var fs = require('fs');
var JavascriptObfuscator = require('javascript-obfuscator');
var currentFileName = '';
// Function create obfuscate
var createObfuscatedFile = function(fileName) {

  fs.readFile(inputDirectory + fileName, "UTF-8", function(err, data) {

    if (err) {
      throw err;
    }

    var obfuscateResult = JavascriptObfuscator.obfuscate(data);
    var fileResult = '';
    if(makeSameName){
      fileResult = outputDirectory + fileName;
    }else{
      var initName = afterfix === '' ? 'obfuscated' : afterfix;      
      fileResult = outputDirectory + fileName.substr(0, fileName.length - 3) + '-' + initName + '.js';
    }

    fs.writeFile(fileResult, obfuscateResult.getObfuscatedCode(), function(err) {
      if (err) {
        return console.log(err);
      }
      console.log('File '+ fileName +' has been Obfuscated');
      console.log('file located at ' + fileResult);
      console.log();
    });
  })
}

// List all files in a directory in Node.js recursively in a synchronous fashion
var getListFiles = function(dir, filelist) {
  var fs = fs || require('fs'),
    files = fs.readdirSync(dir);
  filelist = filelist || [];
  files.forEach(function(file) {
    if (fs.statSync(dir + '/' + file).isDirectory()) {
      filelist = getListFiles(dir + '/' + file, filelist);
    }
    else {
      filelist.push(file);
    }
  });
  return filelist;
};

//Create Obfuscated File
var listFileNames = getListFiles(inputDirectory);
for (var i = 0; i < listFileNames.length; i++) {
  currentFileName = listFileNames[i];
  createObfuscatedFile(currentFileName);
}